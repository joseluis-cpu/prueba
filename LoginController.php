<?php

namespace auditoria\Http\Controllers;

use Illuminate\Http\Request;
use auditoria\Login;
use auditoria\General;

use auditoria\Soap\SoapCliente;
use Validator;


class LoginController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //return view('gnaf_login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $rules = [
            'username' => 'required|max:15',
            'password' => 'required|min:3|max:50',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // It failed
            return redirect('/Login')->withErrors($validator)
                            ->withInput();
        }
        $conexion = SoapCliente::Soap_ObtenerConexion2(config('constants.autenticacion.servicio_soap'));
        $codigo_sistema = 10;

        $arrResultado = Login::ObtenerLista_AutenticacionPrincipal($conexion, $request->username, $request->password, $codigo_sistema);
        $arrResultado = $arrResultado->objspr_sel_autenticacion_usuarioscge;
        
        $arrResultadoParcial = $arrResultado;
        foreach ($arrResultado as $registro) {
            if(isset($registro->rol_codigo) &&  $registro->rol_codigo>0){
                $arrResultadoParcial = $registro;
                break;
            }
        }
        $privilegios = array();
        if($registro == null) $codigo_roles=0;
        else {
            $codigo_roles = $registro->rol_codigo;
        }

        $codAreaUsuario = 0;
        if ($arrResultadoParcial->err_existente == 0) {
            $datosAdicionales =  explode('|', $arrResultadoParcial->err_mensaje);
            $codAreaUsuario =  (int)  $datosAdicionales[3];
        }

        switch($codigo_roles){
            case 37: //GERENTE PRINCIPAL
            $privilegios = array(

                env('MENU_GENERAL'),

                env('PRELIMINAR_CREAR_PROYECTO'),
                env('PRELIMINAR_MODIFICAR_PERSONAL'),
                env('PRELIMINAR_CAMBIAR_ETAPA'),
                env('PRELIMINAR_CHAT'),
                env('PRELIMINAR_ASIGNAR_ACTIVIDAD'),
                //env('PRELIMINAR_CREAR_DOCUMENTOS'),
                //env('PRELIMINAR_GUARDAR_DOCUMENTOS'),
                //env('PRELIMINAR_GUARDAR_ANEXOS'),
                //env('PRELIMINAR_FINALIZAR_DOCUMENTOS'),
                //env('PRELIMINAR_ACTUALIZAR_AVANCE'),

                env('EJECUCION_CREAR_PROYECTO'),
                env('EJECUCION_MODIFICAR_PERSONAL'),
                //env('FINALIZAR_PROYECTO'),
                env('EJECUCION_CHAT'),
                env('EJECUCION_ASIGNAR_ACTIVIDAD'),
                //env('EJECUCION_CREAR_DOCUMENTOS'),
                //env('EJECUCION_GUARDAR_DOCUMENTOS'),
                //env('EJECUCION_GUARDAR_ANEXOS'),
                //env('EJECUCION_FINALIZAR_DOCUMENTOS'),
                //env('EJECUCION_ACTUALIZAR_AVANCE')
                env('MONITOR_GERENCIAL'),
                //env('MONITOR_AVANZADO_GERENCIAL')
            );
            break;
            case 36: //GERENTE DEPARTAMENTAL
            $privilegios = array(
                env('MENU_GENERAL'),
                env('PRELIMINAR_CREAR_PROYECTO'),
                env('PRELIMINAR_MODIFICAR_PERSONAL'),
                //env('PRELIMINAR_CAMBIAR_ETAPA'),
                env('PRELIMINAR_CHAT'),
                //env('PRELIMINAR_ASIGNAR_ACTIVIDAD'),
                //env('PRELIMINAR_CREAR_DOCUMENTOS'),
                //env('PRELIMINAR_GUARDAR_DOCUMENTOS'),
                //env('PRELIMINAR_GUARDAR_ANEXOS'),
                //env('PRELIMINAR_FINALIZAR_DOCUMENTOS'),
                //env('PRELIMINAR_ACTUALIZAR_AVANCE'),
                env('EJECUCION_CREAR_PROYECTO'),
                env('EJECUCION_MODIFICAR_PERSONAL'),
                //env('FINALIZAR_PROYECTO'),
                env('EJECUCION_CHAT'),
                //env('EJECUCION_ASIGNAR_ACTIVIDAD'),
                //env('EJECUCION_CREAR_DOCUMENTOS'),
                //env('EJECUCION_GUARDAR_DOCUMENTOS'),
                //env('EJECUCION_GUARDAR_ANEXOS'),
                //env('EJECUCION_FINALIZAR_DOCUMENTOS'),
                //env('EJECUCION_ACTUALIZAR_AVANCE')
                env('MONITOR_GERENCIAL'),
                //env('MONITOR_AVANZADO_GERENCIAL')
            );
            break;
            case 35: //SUBCONTRALORES
                if($codAreaUsuario == 23)
                {
                    $privilegios = array(
                            //env('MENU_GENERAL'),
                            // env('PRELIMINAR_CREAR_PROYECTO'),
                            // env('PRELIMINAR_MODIFICAR_PERSONAL'),
                            //env('PRELIMINAR_CAMBIAR_ETAPA'),
                            env('PRELIMINAR_CHAT'),
                            env('PRELIMINAR_ASIGNAR_ACTIVIDAD'),
                            //env('PRELIMINAR_CREAR_DOCUMENTOS'),
                            //env('PRELIMINAR_GUARDAR_DOCUMENTOS'),
                            //env('PRELIMINAR_GUARDAR_ANEXOS'),
                            //env('PRELIMINAR_FINALIZAR_DOCUMENTOS'),
                            //env('PRELIMINAR_ACTUALIZAR_AVANCE'),
                            //env('EJECUCION_CREAR_PROYECTO'),
                            //env('EJECUCION_MODIFICAR_PERSONAL'),
                            //env('FINALIZAR_PROYECTO'),
                            env('EJECUCION_CHAT'),
                            //env('EJECUCION_ASIGNAR_ACTIVIDAD'),
                            //env('EJECUCION_CREAR_DOCUMENTOS'),
                            //env('EJECUCION_GUARDAR_DOCUMENTOS'),
                            //env('EJECUCION_GUARDAR_ANEXOS'),
                            //env('EJECUCION_FINALIZAR_DOCUMENTOS'),
                            //env('EJECUCION_ACTUALIZAR_AVANCE')
                            env('MONITOR_GERENCIAL'),
                            //env('MONITOR_AVANZADO_GERENCIAL')
                            env('INSTRUCCION_SUBCONTRALOR_GERENTE_PROY'),
                            env('PROYECTOS_SUBCONTRALORIAS')
                        );
                }
                else{


                    $privilegios = array(
                        env('MENU_GENERAL'),
                    // env('PRELIMINAR_CREAR_PROYECTO'),
                    // env('PRELIMINAR_MODIFICAR_PERSONAL'),
                        //env('PRELIMINAR_CAMBIAR_ETAPA'),
                        env('PRELIMINAR_CHAT'),
                        env('PRELIMINAR_ASIGNAR_ACTIVIDAD'),
                        //env('PRELIMINAR_CREAR_DOCUMENTOS'),
                        //env('PRELIMINAR_GUARDAR_DOCUMENTOS'),
                        //env('PRELIMINAR_GUARDAR_ANEXOS'),
                        //env('PRELIMINAR_FINALIZAR_DOCUMENTOS'),
                        //env('PRELIMINAR_ACTUALIZAR_AVANCE'),
                        //env('EJECUCION_CREAR_PROYECTO'),
                        //env('EJECUCION_MODIFICAR_PERSONAL'),
                        //env('FINALIZAR_PROYECTO'),
                        env('EJECUCION_CHAT'),
                        //env('EJECUCION_ASIGNAR_ACTIVIDAD'),
                        //env('EJECUCION_CREAR_DOCUMENTOS'),
                        //env('EJECUCION_GUARDAR_DOCUMENTOS'),
                        //env('EJECUCION_GUARDAR_ANEXOS'),
                        //env('EJECUCION_FINALIZAR_DOCUMENTOS'),
                        //env('EJECUCION_ACTUALIZAR_AVANCE')
                        env('MONITOR_GERENCIAL'),
                        //env('MONITOR_AVANZADO_GERENCIAL')
                        env('INSTRUCCION_SUBCONTRALOR_GERENTE_PROY'),
                        env("PROYECTOS_SUBCONTRALORIAS")
                        );
                }
            break;
            case 38: //SUBCONTRALORES SCSL y SCAT
            $privilegios = array(
                env('MENU_GENERAL'),
                //env('PRELIMINAR_CREAR_PROYECTO'),
               // env('PRELIMINAR_MODIFICAR_PERSONAL'),
                //env('PRELIMINAR_CAMBIAR_ETAPA'),
                env('PRELIMINAR_CHAT'),
                env('PRELIMINAR_ASIGNAR_ACTIVIDAD'),
                //env('PRELIMINAR_CREAR_DOCUMENTOS'),
                //env('PRELIMINAR_GUARDAR_DOCUMENTOS'),
                //env('PRELIMINAR_GUARDAR_ANEXOS'),
                //env('PRELIMINAR_FINALIZAR_DOCUMENTOS'),
                //env('PRELIMINAR_ACTUALIZAR_AVANCE'),
                //env('EJECUCION_CREAR_PROYECTO'),
                //env('EJECUCION_MODIFICAR_PERSONAL'),
                //env('FINALIZAR_PROYECTO'),
                env('EJECUCION_CHAT'),
                //env('EJECUCION_ASIGNAR_ACTIVIDAD'),
                //env('EJECUCION_CREAR_DOCUMENTOS'),
                //env('EJECUCION_GUARDAR_DOCUMENTOS'),
                //env('EJECUCION_GUARDAR_ANEXOS'),
                //env('EJECUCION_FINALIZAR_DOCUMENTOS'),
                //env('EJECUCION_ACTUALIZAR_AVANCE')
                env('MONITOR_GERENCIAL'),
                //env('MONITOR_AVANZADO_GERENCIAL')
                env('INSTRUCCION_SUBCONTRALOR_GERENTE_PROY'),
                env("PROYECTOS_SUBCONTRALORIAS")
            );
            break;
            case 33: //EJECUTIVOS
            $privilegios = array(
                env('MENU_GENERAL'),
               // env('PRELIMINAR_CREAR_PROYECTO'),
               // env('PRELIMINAR_MODIFICAR_PERSONAL'),
                //env('PRELIMINAR_CAMBIAR_ETAPA'),
                //env('PRELIMINAR_CHAT'),
                env('PRELIMINAR_ASIGNAR_ACTIVIDAD'),
                //env('PRELIMINAR_CREAR_DOCUMENTOS'),
                //env('PRELIMINAR_GUARDAR_DOCUMENTOS'),
                //env('PRELIMINAR_GUARDAR_ANEXOS'),
                //env('PRELIMINAR_FINALIZAR_DOCUMENTOS'),
                //env('PRELIMINAR_ACTUALIZAR_AVANCE'),
                //env('EJECUCION_CREAR_PROYECTO'),
                //env('EJECUCION_MODIFICAR_PERSONAL'),
                //env('FINALIZAR_PROYECTO'),
                //env('EJECUCION_CHAT'),
                //env('EJECUCION_ASIGNAR_ACTIVIDAD'),
                //env('EJECUCION_CREAR_DOCUMENTOS'),
                //env('EJECUCION_GUARDAR_DOCUMENTOS'),
                //env('EJECUCION_GUARDAR_ANEXOS'),
                //env('EJECUCION_FINALIZAR_DOCUMENTOS'),
                //env('EJECUCION_ACTUALIZAR_AVANCE')
                env('MONITOR_GERENCIAL'),
                env('MONITOR_AVANZADO_GERENCIAL')
               
            );
            break;
            default:
            $privilegios = array(env('MENU_GENERAL'));

        }
        session()->put('privilegios',$privilegios);
        session()->put('redireccionar','');
        $arrResultado=  $arrResultadoParcial ;
        $validator->after(function ($validator) use ( $arrResultado ) {
            if ($arrResultado->err_existente != 0) {
                $validator->errors()->add('usuario', $arrResultado->err_mensaje);
            }
        });

        if ($validator->fails()) {
            return redirect('/Login')->withErrors($validator)
                            ->withInput();
        }
        $arrResultadoLogin = Login::ObtenerLista_ComprobarAutenticacion($conexion, $arrResultado->codigo_session_operacional);
        $arrResultadoLogin = $arrResultadoLogin->objspr_sel_autenticacion_usuarioscge;

       $unidad =  explode("|", $arrResultado->err_mensaje);
       $arrResultado->codigo_unidad =  $unidad[1];
       $arrResultado->descripcion_unidad =  $unidad[0];
       $arrResultado->codigo_area =  $unidad[3];
       $arrResultado->descripcion_area =  $unidad[2];

        session()->put('usuario_login', $arrResultado);
        session()->put('usuario_acceso', $arrResultadoLogin);
       session()->put('loggg',$request->username);
     /*  if(
           $request->username == "4782908"
           ||  $request->username =="9887630"
           ||  $request->username =="4756394"
           ||  $request->username =="3349353"
           ||  $request->username =="4811978"
           )
        {
            session()->put('canal_test','GRUPO1');
        }

        if(
            $request->username == "3977702"||  $request->username ==   "1907219"
            )
         {
             session()->put('canal_test','GRUPO2');
         }*/

        $arrRoles = array();

        //return redirect('Proyectos');
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

    }

    public function logOut() {
        session()->flush();
        return redirect('Login');
    }

}
